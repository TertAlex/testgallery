package com.sentiel.testgallery;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class ImageAdapter extends BaseAdapter implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static String IMAGES = "images";
    public static String ID = "id";
    public static String NUMBER = "number";
    public static String URL = "url";

    private Context context;
    int curNumber, nextNumber, prevNumber;
    ImageSwitcher imageSwitcher;
    SharedPreferences prefs;

    public ImageAdapter(Context context, String jsonAssets) {
        this.context = context;

        this.prefs = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
        this.prefs.registerOnSharedPreferenceChangeListener(this);
        try {
            if (!AppPrefs.getIsAddImages()) {
                JSONArray jsonArray = new JSONObject(loadJSONFromAsset(jsonAssets)).getJSONArray(IMAGES);
                for (int i = 0; i < jsonArray.length(); i++) {
                    MyImage myImage = parseJsonImage(jsonArray.getJSONObject(i));
                    if (myImage != null) {
                        DataBaseHelper.addImageToDataBase(myImage);
                    }
                }
                AppPrefs.setIsAddImages(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void prepareImages() {
        FilterData filterData = new FilterData();
        filterData.isFav = AppPrefs.isShowFav();

        MyImage myImage = DataBaseHelper.getFirstImage(filterData);
        if (myImage != null) {
            this.curNumber = myImage.getNumber();
            ResourceHelper.prepareImage(0, myImage.getUrl());
        }

        loadNextImage(1, true);
        loadPrevImage(2, true);
    }

    public void setImageSwitcher(ImageSwitcher imageSwitcher) {
        this.imageSwitcher = imageSwitcher;
    }

    private void loadNextImage(int position, boolean isFirst) {
        FilterData filterData = new FilterData();
        filterData.isFav = AppPrefs.isShowFav();

        MyImage myImage;
        if (AppPrefs.isShowRandom()) {
            myImage = DataBaseHelper.getRandomImage(filterData);
        } else {
            myImage = DataBaseHelper.getNextImage(isFirst ? this.curNumber : this.nextNumber, filterData);
        }
        if (myImage != null) {
            this.nextNumber = myImage.getNumber();
            //     Log.e(MainActivity.LOG, "loadNextImage curNumber " + this.curNumber + " nextNumber " + nextNumber + " prevNumber " + prevNumber + " position " + position);
            ResourceHelper.prepareImage(position, myImage.getUrl());
        }
    }

    private void loadPrevImage(int position, boolean isFirst) {
        FilterData filterData = new FilterData();
        filterData.isFav = AppPrefs.isShowFav();

        MyImage myImage;
        if (AppPrefs.isShowRandom()) {
            myImage = DataBaseHelper.getRandomImage(filterData);
        } else {
            myImage = DataBaseHelper.getPrevImage(isFirst ? this.curNumber : this.prevNumber, filterData);
        }
        if (myImage != null) {
            this.prevNumber = myImage.getNumber();
            //     Log.e(MainActivity.LOG, "loadPrevImage curNumber " + this.curNumber + " nextNumber " + nextNumber + " prevNumber " + prevNumber + " position " + position);
            ResourceHelper.prepareImage(position, myImage.getUrl());
        }
    }

    @Override
    public int getCount() {
        return DataBaseHelper.getCount();
    }

    @Override
    public MyImage getItem(int position) {
        return DataBaseHelper.getImageByPosition(position);
    }

    @Override
    public long getItemId(int position) {
        return DataBaseHelper.getImageByPosition(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView != null) {
            imageView = (ImageView) convertView;
        } else {
            imageView = new ImageView(this.context);
        }
        imageView.setImageBitmap(ResourceHelper.getImage(position));
        return imageView;
    }

    public MyImage getCurMyImage() {
        return DataBaseHelper.getImageByNumber(this.curNumber);
    }

    public void prepareNextImage(int nextImage, int prevImage) {
        ResourceHelper.deleteFile(nextImage);
        //   ResourceHelper.deleteFile(this.curNumber);
        this.prevNumber = this.curNumber;
        this.curNumber = this.nextNumber;
        loadNextImage(nextImage, false);
        //    loadPrevImage(prevImage, true);
        Log.e(MainActivity.LOG, "prepareNextImage  nextImage " + nextImage + " prevImage " + prevImage);
    }

    public void preparePrevImage(int nextImage, int prevImage) {
        ResourceHelper.deleteFile(prevImage);
        //     ResourceHelper.deleteFile(this.curNumber);
        this.nextNumber = this.curNumber;
        this.curNumber = this.prevNumber;
        loadPrevImage(prevImage, false);
        //    loadNextImage(nextImage, true);
        Log.e(MainActivity.LOG, "preparePrevImage  nextImage " + nextImage + " prevImage " + prevImage);
    }

    public void setImageToSwitcher(int imageNumber) {
        MyImage myImage = DataBaseHelper.getImageByNumber(this.curNumber);
        ResourceHelper.setImage(context, this.imageSwitcher, imageNumber, myImage.getUrl());
    }

    private String loadJSONFromAsset(String jsonAssets) {
        String json = null;
        try {
            InputStream is = MyApp.getContext().getAssets().open(jsonAssets);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public MyImage parseJsonImage(JSONObject jsonObject) {
        try {
            return new MyImage.MyImageBuilder()
                    .server_id(jsonObject.getLong(ID))
                    .number(jsonObject.getInt(NUMBER))
                    .url(jsonObject.getString(URL))
                    .build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        for (int i = 0; i < MyGallery.countViews; i++) {
            ResourceHelper.deleteFile(i);
        }
        prepareImages();
    }
}
