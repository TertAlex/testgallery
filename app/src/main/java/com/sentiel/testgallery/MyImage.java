package com.sentiel.testgallery;

public class MyImage {
    private long id;
    private long server_id;
    private int number;
    private String url;
    private boolean isFav = false;
    private String comment = "";

    public MyImage(MyImageBuilder myImageBuilder) {
        this.id = myImageBuilder.id;
        this.server_id = myImageBuilder.server_id;
        this.number = myImageBuilder.number;
        this.url = myImageBuilder.url;
        this.isFav = myImageBuilder.isFav;
        this.comment = myImageBuilder.comment;
    }

    public long getId() {
        return id;
    }
    public long getServer_id() {
        return server_id;
    }
    public int getNumber() {
        return number;
    }
    public String getUrl() {
        return url;
    }
    public boolean isFav() {
        return isFav;
    }
    public String getComment() {
        return comment;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    public static class MyImageBuilder{
        private long id;
        private long server_id;
        private int number;
        private String url;
        private boolean isFav = false;
        private String comment = "";

        public MyImageBuilder id(long id){
            this.id = id;
            return this;
        }
        public MyImageBuilder server_id(long server_id){
            this.server_id = server_id;
            return this;
        }
        public MyImageBuilder number(int number){
            this.number = number;
            return this;
        }
        public MyImageBuilder url(String url){
            this.url = url;
            return this;
        }
        public MyImageBuilder isFav(boolean isFav){
            this.isFav = isFav;
            return this;
        }
        public MyImageBuilder comment(String comment){
            this.comment = comment;
            return this;
        }

        public MyImage build(){
            return new MyImage(this);
        }
    }
}
