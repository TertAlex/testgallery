package com.sentiel.testgallery;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener {
    public static String LOG = "TestGalleryLogs";

    MyGallery myGallery;
    GestureDetector gd;
    SharedPreferences  prefs;

    Timer timer;
    TimerTask tTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.myGallery = (MyGallery) findViewById(R.id.myGallery);
        this.myGallery.setBtnFav((ToggleButton) findViewById(R.id.tbFav));
        this.myGallery.setTvComment((TextView) findViewById(R.id.tvComment));

        this.prefs = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
        this.prefs.registerOnSharedPreferenceChangeListener(this);
        AppPrefs.loadSetting(this.prefs);

        ImageAdapter imageAdapter = new ImageAdapter(this, "data.json");
        this.myGallery.setImageAdapter(imageAdapter);

        this.gd = new GestureDetector(this, simpleOnGestureListener);

        findViewById(R.id.btnNext).setOnClickListener(this);
        findViewById(R.id.btnPrev).setOnClickListener(this);

        this.startTimer();
    }

    @Override
    protected void onDestroy() {
        this.prefs.unregisterOnSharedPreferenceChangeListener(this);
        this.myGallery.destroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.prefs:
                Intent i = new Intent(this, AppPrefs.class);
                startActivity(i);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gd.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    GestureDetector.SimpleOnGestureListener simpleOnGestureListener = new GestureDetector.SimpleOnGestureListener() {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 100;

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            try {
                if (Math.abs(event1.getY() - event2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // справа налево
                if (event1.getX() - event2.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    next();
                } else if (event2.getX() - event1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    // слева направо
                    prev();
                }
            } catch (Exception e) {
                // nothing
                return true;
            }
            return true;
        }
    };

    private void startTimer() {
        if (AppPrefs.isSlideShow()) {
            this.timer = new Timer();
            if (this.tTask != null) this.tTask.cancel();
            if (AppPrefs.getInterval() > 0) {
                this.tTask = new TimerTask() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                next();
                            }
                        });
                    }
                };
                this.timer.schedule(this.tTask, 500, AppPrefs.getInterval() * 1000);
            }
        }else{
            if (this.tTask != null) this.tTask.cancel();
        }
    }

    private void next(){
        this.myGallery.nextImage();
    }
    private void prev(){
        this.myGallery.prevImage();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.startTimer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNext:
                next();
                break;
            case R.id.btnPrev:
                prev();
                break;
        }
    }
}
