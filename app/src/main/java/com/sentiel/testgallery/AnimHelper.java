package com.sentiel.testgallery;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class AnimHelper {
    private static int curAnim;

    public static void SetCurAnim(int currentAnim){
        curAnim = currentAnim;
    }

    public static Animation GetInAnimation(boolean isLeft){
        switch (curAnim){
            case 0:
                return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.fade_in);
            case 1:
                if (isLeft) {
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.flip_in_left);
                }else{
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.flip_in_right);
                }
            case 2:
                if (isLeft) {
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.scale_in_left);
                }else{
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.scale_in_right);
                }
        }
        Animation inAnimation = new AlphaAnimation(0, 1);
        inAnimation.setDuration(2000);
        return inAnimation;
    }

    public static Animation GetOutAnimation(boolean isLeft){
        switch (curAnim){
            case 0:
                return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.fade_out);
            case 1:
                if (isLeft){
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.flip_out_left);
                }else{
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.flip_out_right);
                }
            case 2:
                if (isLeft) {
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.scale_out_left);
                }else{
                    return AnimationUtils.loadAnimation(MyApp.getContext(), R.anim.scale_out_right);
                }
        }
        Animation outAnimation = new AlphaAnimation(1, 0);
        outAnimation.setDuration(2000);
        return outAnimation;
    }


}
