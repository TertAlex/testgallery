package com.sentiel.testgallery;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.List;

public class AppPrefs extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG_IS_ADD_IMAGES = "isAddImages";
    private static final String DEFAULT_INTERVAL = "2";

    private static boolean isShowRandom;
    private static boolean isShowFav;
    private static long interval;
    private static boolean isSlideShow;

    public void onBuildHeaders(List<Header> target) {
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new AppMainSettingFragment()).commit();
    }
    @Override
    protected boolean isValidFragment(String fragmentName) {
        return AppMainSettingFragment.class.getName().equals(fragmentName)
                || super.isValidFragment(fragmentName);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.loadSetting(sharedPreferences);
    }

    public static void loadSetting(SharedPreferences sharedPreferences) {
        isShowRandom = sharedPreferences.getBoolean("show_random", false);
        isShowFav = sharedPreferences.getBoolean("show_fav", false);
        isSlideShow = sharedPreferences.getBoolean("slide_show", false);
        interval = Long.parseLong(sharedPreferences.getString("interval", DEFAULT_INTERVAL));

        AnimHelper.SetCurAnim(Integer.parseInt(sharedPreferences.getString("anim", "0")));
    }

    public static class AppMainSettingFragment extends PreferenceFragment  {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.app_prefs);
        }
    }

    public static void setIsAddImages(boolean isAddImages){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(TAG_IS_ADD_IMAGES, isAddImages);
        editor.apply();
    }
    public static boolean getIsAddImages(){
        return PreferenceManager.getDefaultSharedPreferences(MyApp.getContext()).getBoolean(TAG_IS_ADD_IMAGES, false);
    }

    public static boolean isShowRandom() {
        return isShowRandom;
    }
    public static boolean isShowFav() {
        return isShowFav;
    }
    public static long getInterval() {
        return interval;
    }
    public static boolean isSlideShow() {
        return isSlideShow;
    }
}
