package com.sentiel.testgallery;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DataBaseHelper extends SQLiteOpenHelper {
    //константы имя и версия базы
    private static final String DATABASE_NAME = "testgallerydb.db";
    private static final int DATABASE_VERSION = 1;

    //константы таблицы и полей базы
    public static final String TABLE = "images";
    public static final String ID = "_id";
    public static final String SERVER_ID = "server_id";
    public static final String NUMBER = "number";
    public static final String URL = "url";
    public static final String IS_FAV = "is_fav";
    public static final String COMMENT = "comment";

    //создание базы с нуля
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " ( " + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SERVER_ID + " INTEGER, "
            + NUMBER + " INTEGER, "
            + URL + " TEXT, "
            + IS_FAV + " INTEGER, "
            + COMMENT + " TEXT);";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long addImageToDataBase(MyImage myImage) {
        long _id = 0;
        try {
            // создаем объект для данных
            ContentValues cv = new ContentValues();
            cv.put(SERVER_ID, myImage.getServer_id());
            cv.put(NUMBER, myImage.getNumber());
            cv.put(URL, myImage.getUrl());
            cv.put(IS_FAV, myImage.isFav() ? 1 : 0);
            cv.put(COMMENT, myImage.getComment());
            // вставляем запись и получаем ее ID
            _id = MyApp.getDataBaseHelper().getWritableDatabase().insert(TABLE, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _id;
    }

    public static boolean updImageInDataBase(MyImage myImage) {
        try {
            String selection = ID + " = ?";
            String[] selectionArgs = new String[]{String.valueOf(myImage.getId())};
            // создаем объект для данных
            ContentValues cv = new ContentValues();
            cv.put(ID, myImage.getId());
            cv.put(SERVER_ID, myImage.getServer_id());
            cv.put(NUMBER, myImage.getNumber());
            cv.put(URL, myImage.getUrl());
            cv.put(IS_FAV, myImage.isFav() ? 1 : 0);
            cv.put(COMMENT, myImage.getComment());
            // вставляем запись и получаем ее ID
            MyApp.getDataBaseHelper().getWritableDatabase().update(TABLE, cv, selection, selectionArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private static String addFilterSel(String selection, FilterData filterData){
        if (filterData.isFav){
            if (selection != null && !selection.isEmpty()){
                selection += " AND ";
            }else {
                selection = "";
            }
            selection += IS_FAV + " = ?";
        }
        return selection;
    }

    private static String[] addFilterSelArgs(String[] selectionArgs, FilterData filterData){
        ArrayList<String> selectionArgsAR = new ArrayList<>();
        Collections.addAll(selectionArgsAR, selectionArgs);
        if (filterData.isFav){
            selectionArgsAR.add(String.valueOf(1));
        }
        if (selectionArgsAR.isEmpty()){
            return null;
        }else {
            return selectionArgsAR.toArray(new String[selectionArgsAR.size()]);
        }
    }

    public static MyImage getNextImage(int number, FilterData filterData) {
        String selection = addFilterSel(NUMBER + " > ?", filterData);
        String[] selectionArgs = addFilterSelArgs(new String[]{String.valueOf(number)}, filterData);
        String orderBy = NUMBER + " ASC LIMIT 1";
        MyImage myImage = getImage(selection, selectionArgs, orderBy);

        if (myImage == null) {
            myImage = getFirstImage(filterData);
        }

        return myImage;
    }

    public static MyImage getPrevImage(int number, FilterData filterData) {
        String selection = addFilterSel(NUMBER + " < ?", filterData);
        String[] selectionArgs = addFilterSelArgs(new String[]{String.valueOf(number)}, filterData);
        String orderBy = NUMBER + " DESC LIMIT 1";
        MyImage myImage = getImage(selection, selectionArgs, orderBy);

        if (myImage == null) {
            myImage = getLastImage(filterData);
        }

        return myImage;
    }

    public static MyImage getFirstImage(FilterData filterData) {
        String selection = addFilterSel(null, filterData);
        String[] selectionArgs = addFilterSelArgs(new String[]{}, filterData);
        String orderBy = NUMBER + " ASC LIMIT 1";
        return getImage(selection, selectionArgs, orderBy);
    }

    public static MyImage getLastImage(FilterData filterData) {
        String selection = addFilterSel(null, filterData);
        String[] selectionArgs = addFilterSelArgs(new String[]{}, filterData);
        String orderBy = NUMBER + " DESC LIMIT 1";
        return getImage(selection, selectionArgs, orderBy);
    }

    public static MyImage getRandomImage(FilterData filterData) {
        String selection = addFilterSel(null, filterData);
        String[] selectionArgs = addFilterSelArgs(new String[]{}, filterData);
        String orderBy = " RANDOM() LIMIT 1";
        return getImage(selection, selectionArgs, orderBy);
    }

    public static MyImage getImage(String selection, String[] selectionArgs, String orderBy) {
        MyImage myImage = null;
     //   Log.e(MainActivity.LOG, " selection " + selection + " selectionArgs " + Arrays.toString(selectionArgs) + " orderBy " + orderBy);
        try {
            Cursor cursor = MyApp.getDataBaseHelper().getReadableDatabase().query(TABLE,
                    null, selection, selectionArgs, null, null, orderBy);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    myImage = getImageFromCursor(cursor);
                    cursor.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myImage;
    }

    public static int getCount() {
        int count = 0;
        try {
            Cursor cursor = MyApp.getDataBaseHelper().getReadableDatabase().query(TABLE, null, null, null, null, null, null);
            if (cursor != null) {
                count = cursor.getCount();
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public static MyImage getImageById(long id) {
        String selection = ID + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(id)};
        return getImage(selection, selectionArgs, null);
    }

    public static MyImage getImageByNumber(long number) {
        String selection = NUMBER + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(number)};
        return getImage(selection, selectionArgs, null);
    }

    public static MyImage getImageByPosition(int position) {
        MyImage myImage = null;
        try {
            Cursor cursor = MyApp.getDataBaseHelper().getReadableDatabase().query(TABLE, null, null, null, null, null, null);
            if (cursor.moveToPosition(position)) {
                myImage = getImageFromCursor(cursor);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myImage;
    }

    public static MyImage getImageFromCursor(Cursor cursor) {
        return new MyImage.MyImageBuilder()
                .id(cursor.getLong(cursor.getColumnIndex(ID)))
                .server_id(cursor.getLong(cursor.getColumnIndex(SERVER_ID)))
                .number(cursor.getInt(cursor.getColumnIndex(NUMBER)))
                .url(cursor.getString(cursor.getColumnIndex(URL)))
                .isFav(cursor.getInt(cursor.getColumnIndex(IS_FAV)) == 1)
                .comment(cursor.getString(cursor.getColumnIndex(COMMENT)))
                .build();
    }
}