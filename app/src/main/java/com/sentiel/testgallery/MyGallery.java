package com.sentiel.testgallery;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher;

public class MyGallery extends ImageSwitcher implements ViewSwitcher.ViewFactory, View.OnClickListener, SharedPreferences.OnSharedPreferenceChangeListener {
    public static int countViews = 3;
    ImageAdapter imageAdapter;
    ToggleButton tbFav;
    TextView tvComment;
    Context context;
    int curImage = 0;
    SharedPreferences prefs;

    public MyGallery(Context context) {
        super(context);
        this.init(context);
    }

    public MyGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    private void init(Context context) {
        this.context = context;
        this.setFactory(this);

        this.prefs = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
        this.prefs.registerOnSharedPreferenceChangeListener(this);
    }

    public void setAnimation(boolean isLeft) {
        this.setInAnimation(AnimHelper.GetInAnimation(isLeft));
        this.setOutAnimation(AnimHelper.GetOutAnimation(isLeft));
    }

    public void setImageAdapter(ImageAdapter imageAdapter) {
        this.imageAdapter = imageAdapter;
        this.imageAdapter.setImageSwitcher(this);
        this.imageAdapter.prepareImages();
        setImage(this.curImage);
    }

    public void setBtnFav(ToggleButton tbFav) {
        this.tbFav = tbFav;
        this.tbFav.setOnClickListener(this);
    }

    public void setTvComment(TextView tvComment) {
        this.tvComment = tvComment;
    }

    private void setImage(int imageNumber) {
        this.imageAdapter.setImageToSwitcher(imageNumber);

        MyImage myImage = this.imageAdapter.getCurMyImage();
        if (myImage != null) {
            if (myImage.isFav()) {
                showComment(myImage.getComment());
            }else{
                clearComment();
            }
            this.tbFav.setChecked(myImage.isFav());
        }
    }

    private void clearComment(){
        this.tvComment.setText("");
        this.tvComment.setVisibility(GONE);
    }

    private void showComment(String comment){
        if (comment != null && comment != "") {
            this.tvComment.setText(comment);
            this.tvComment.setVisibility(VISIBLE);
        }
    }

    public void nextImage() {
        this.curImage = getNextImage();
        this.imageAdapter.prepareNextImage(getNextImage(), getPrevImage());
        this.setAnimation(true);
        setImage(this.curImage);
    }

    public void prevImage() {
        this.curImage = getPrevImage();
        this.imageAdapter.preparePrevImage(getNextImage(), getPrevImage());
        this.setAnimation(false);
        setImage(this.curImage);
    }

    private int getNextImage() {
        return this.curImage + 1 >= countViews ? 0 : this.curImage + 1;
    }
    private int getPrevImage() {
        return this.curImage - 1 < 0 ? countViews - 1 : this.curImage - 1;
    }

    @Override
    public View makeView() {
        ImageView iView = new ImageView(this.context);
        iView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iView.setLayoutParams(new ImageSwitcher.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        iView.setBackgroundColor(Color.TRANSPARENT);
        return iView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tbFav:
                MyImage myImage = this.imageAdapter.getCurMyImage();
                myImage.setFav(this.tbFav.isChecked());
                DataBaseHelper.updImageInDataBase(myImage);
                if (this.tbFav.isChecked()) {
                    showDialogEditComment();
                }else{
                    clearComment();
                }
                break;
        }
    }

    private void saveComment(String comment) {
        MyImage myImage = this.imageAdapter.getCurMyImage();
        myImage.setComment(comment);
        showComment(myImage.getComment());
        DataBaseHelper.updImageInDataBase(myImage);
    }

    private void showDialogEditComment() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this.context);
        ViewGroup viewGroup = (ViewGroup) this.getParent();
        View promptsView = li.inflate(R.layout.dialog_edit_comment, viewGroup, false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.etComment);
        MyImage myImage = this.imageAdapter.getCurMyImage();
        userInput.setText(myImage.getComment());

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(this.context.getString(R.string.save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                saveComment((userInput.getText().toString()));
                            }
                        })
                .setNegativeButton(this.context.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        userInput.setSelection(userInput.getText().length());

        // show it
        alertDialog.show();
    }

    public void destroy(){
        for (int i = 0; i < countViews; i++){
            ResourceHelper.deleteFile(i);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        setImage(this.curImage);
    }
}
