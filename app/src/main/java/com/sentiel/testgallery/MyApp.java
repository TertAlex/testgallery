package com.sentiel.testgallery;

import android.app.Application;
import android.content.Context;

public class MyApp extends Application {
    private static MyApp instance;
    private static DataBaseHelper dataBaseHelper;

    public MyApp() {
        instance = this;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        dataBaseHelper = new DataBaseHelper(instance);
    }

    public static Context getContext() {
        return instance;
    }

    public static DataBaseHelper getDataBaseHelper(){
        return dataBaseHelper;
    }

    @Override
    public void onTerminate() {
        dataBaseHelper.close();
        super.onTerminate();
    }
}
