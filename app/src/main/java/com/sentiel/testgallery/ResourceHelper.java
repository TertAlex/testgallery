package com.sentiel.testgallery;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageSwitcher;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class ResourceHelper {
    static Context context;

    static DownloadImageTaskNow downloadImageTaskNow;

    private static int mWidth;
    private static int mHeight;

    static {
        DisplayMetrics metrics = MyApp.getContext().getResources().getDisplayMetrics();
        mWidth = metrics.widthPixels;
        mHeight = metrics.heightPixels;
    }

    public static void prepareImage(int curImage, String url) {
        new DownloadImageTask(curImage).execute(url);
    }

    public static Bitmap getImage(int imageNumber) {
        return loadImageFromInternalStorageJPG(imageNumber);
    }

    public static void setImage(Context context1, ImageSwitcher imageSwitcher, int imageNumber, String url) {
        context = context1;
        if (downloadImageTaskNow != null) {
            downloadImageTaskNow.cancel(true);
        }

        Bitmap bmLoad = loadImageFromInternalStorageJPG(imageNumber);

        if (bmLoad != null) {
            imageSwitcher.setImageDrawable(new BitmapDrawable(MyApp.getContext().getResources(), bmLoad));
        } else {
            Bitmap bitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            imageSwitcher.setImageDrawable(new BitmapDrawable(MyApp.getContext().getResources(), bitmap));

            downloadImageTaskNow = new DownloadImageTaskNow(imageSwitcher, imageNumber);
            downloadImageTaskNow.execute(url);
        }
    }

    private static boolean saveImageToInternalStorageJPG(final int imageNumber, final Bitmap image) {
        try {
            FileOutputStream fos = MyApp.getContext().openFileOutput(imageNumber + ".jpg", Context.MODE_PRIVATE);
            image.compress(Bitmap.CompressFormat.JPEG, 50, fos);
            fos.close();
            image.recycle();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static Bitmap loadImageFromInternalStorageJPG(int imageNumber) {
        Bitmap thumbnail = null;
        try {
            File filePath = MyApp.getContext().getFileStreamPath(imageNumber + ".jpg");
            if (filePath.exists()) {
                FileInputStream fi = new FileInputStream(filePath);
                thumbnail = BitmapFactory.decodeStream(fi);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return thumbnail;
    }

    public static boolean deleteFile(int imageNumber) {
        File file = MyApp.getContext().getFileStreamPath(imageNumber + ".jpg");
        Log.e(MainActivity.LOG, "deleteFile " + imageNumber);
        return file.delete();
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Void> {
        int imageNumber;
        String imageName;

        public DownloadImageTask(int imageNumber) {
            this.imageNumber = imageNumber;
        }

        protected Void doInBackground(String... urls) {
            String urldisplay = urls[0];
            imageName = urldisplay;
            try {
                Bitmap bmLoad = BitmapFactory.decodeStream((InputStream) new URL(urldisplay).getContent());
                saveImageToInternalStorageJPG(imageNumber, bmLoad);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private static class DownloadImageTaskNow extends AsyncTask<String, String, Bitmap> {
        int imageNumber;
        String imageName;
        ImageSwitcher imageSwitcher;
        private ProgressDialog pDialog;

        public DownloadImageTaskNow(ImageSwitcher imageSwitcher, int imageNumber) {
            this.imageSwitcher = imageSwitcher;
            this.imageNumber = imageNumber;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            onCreateDialog();
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            imageName = urldisplay;
            Bitmap bmLoad = null;
            int count;

            try {
                URL url = new URL(urldisplay);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = MyApp.getContext().openFileOutput(9 + ".jpg", Context.MODE_PRIVATE);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

                bmLoad = loadImageFromInternalStorageJPG(9);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bmLoad;
        }

        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                imageSwitcher.setImageDrawable(new BitmapDrawable(MyApp.getContext().getResources(), bitmap));
            }
            pDialog.dismiss();
        }

        @Override
        protected void onCancelled() {
            pDialog.dismiss();
            super.onCancelled();
        }

        protected Dialog onCreateDialog() {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(MyApp.getContext().getResources().getString(R.string.download_file));
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();
            return pDialog;
        }
    }
}
